<div class="multiselect-wrapper">
    <!-- select button -->
    <div class="button handler-btn">
        <a class="handler-btn-text">0</a>
    </div>
    <div class="button action-btn">
        <a class="action-btn-text">Execute</a>
    </div>
    <!-- select option popup -->
    <div class="select-popup">
        <ul class="ui-listview">
            <li><a href="#" class="select-all-btn">Select all</a></li>
            <li><a href="#" class="deselect-all-btn">Deselect all</a></li>
        </ul>
    </div>
</div>