{{#files}}
<li>
    <a href="#" data-directory="{{this.isDirectory}}" data-name="{{this.name}}">
        {{?this.isDirectory}}
        <img src="images/create-folder.png"/>
        {{/this.isDirectory}}
        {{?this.isFile}}
        <img src="images/create-file.png"/>
        {{/this.isFile}}
        {{this.name}}
    </a>
</li>
{{/files}}
