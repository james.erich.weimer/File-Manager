/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define, document*/

/**
 * File info view module.
 *
 * @module views/file-info
 * @requires {@link models/state}
 * @requires {@link helpers/page}
 * @namespace views/file-info
 */
define('views/file-info',
        ['models/state', 'helpers/page'], function fileInfo(require) {
    'use strict';

    /**
     * Application state module.
     *
     * @memberof views/file-info
     * @private
     * @type {Module}
     */
    var state = require('models/state'),

        /**
         * Page helper module.
         *
         * @memberof views/file-info
         * @private
         * @type {Module}
         */
        pageHelper = require('helpers/page'),

        /**
         * Reference to the name element.
         *
         * @memberof views/file-info
         * @private
         * @type {HTMLElement}
         */
        nameEl = null,

        /**
         * Reference to the location element.
         *
         * @memberof views/file-info
         * @private
         * @type {HTMLElement}
         */
        locationEl = null,

        /**
         * Reference to the size element.
         *
         * @memberof views/file-info
         * @private
         * @type {HTMLElement}
         */
        sizeEl = null,

        /**
         * Reference to the creation date element.
         *
         * @memberof views/file-info
         * @private
         * @type {HTMLElement}
         */
        creationDateEl = null,

        /**
         * Reference to the modification date element.
         *
         * @memberof views/file-info
         * @private
         * @type {HTMLElement}
         */
        modificationDateEl = null,

        /**
         * Reference to the read-only element.
         *
         * @memberof views/file-info
         * @private
         * @type {HTMLElement}
         */
        readonlyEl = null,

        /**
         * Reference to the page element.
         *
         * @memberof views/file-info
         * @private
         * @type {HTMLElement}
         */
        page = null,

        /**
         * Reference to the title element.
         *
         * @memberof views/file-info
         * @private
         * @type {HTMLElement}
         */
        title = null;

    /**
     * Formats the specified Date object to the following form:
     * yyyy-mm-dd hh:mm:ss
     *
     * Returns formatted value.
     *
     * @memberof views/file-info
     * @private
     * @param {Date} date
     * @returns {string}
     */
    function formatDate(date) {
        return date.toISOString().replace('T', '\n').substring(0, 19);
    }

    /**
     * Formats file size value specified as a parameter.
     *
     * Returns formatted value extended with proper unit string.
     *
     * @memberof views/file-info
     * @private
     * @param {number} fileSize
     * @returns {string}
     */
    function formatSize(fileSize) {
        var thresholds = [1024, 1024 * 1024, 1024 * 1024 * 1024],
            units = ['B', 'kB', 'MB'],
            i = 0,
            length = thresholds.length;

        fileSize = fileSize || 0;

        for (; i < length; i += 1) {
            if (fileSize < thresholds[i]) {
                return (fileSize * 1024 / thresholds[i]).toFixed(2) +
                    ' ' + units[i];
            }
        }

        return (fileSize / thresholds[length - 1]).toFixed(2) + ' GB';
    }

    /**
     * Handles pagebeforeshow event.
     *
     * @memberof views/file-info
     * @private
     */
    function onPageBeforeShow() {
        var file = state.getSelectedFile();

        pageHelper.resetScroll(page);

        if (file.isDirectory) {
            title.innerText = 'Directory info';
            page.classList.add('directory');
        } else {
            title.innerText = 'File info';
            page.classList.remove('directory');
        }

        nameEl.innerText = file.name;
        locationEl.innerText = file.path;
        sizeEl.innerText = formatSize(file.fileSize);
        creationDateEl.innerText = formatDate(file.created);
        modificationDateEl.innerText = formatDate(file.modified);
        readonlyEl.innerText = file.readOnly ? 'Yes' : 'No';
    }

    /**
     * Registers event listeners.
     *
     * @memberof views/file-info
     * @private
     */
    function bindEvents() {
        page.addEventListener('pagebeforeshow', onPageBeforeShow);
    }

    /**
     * Initializes module.
     *
     * @memberof views/file-info
     * @public
     */
    function init() {
        page = document.getElementById('file-info');
        title = page.querySelector('.ui-title');
        nameEl = document.getElementById('file-info-name');
        locationEl = document.getElementById('file-info-location');
        sizeEl = document.getElementById('file-info-size');
        creationDateEl = document.getElementById('file-info-created');
        modificationDateEl = document.getElementById('file-info-modified');
        readonlyEl = document.getElementById('file-info-readonly');
        bindEvents();
    }

    return {
        init: init
    };
});
