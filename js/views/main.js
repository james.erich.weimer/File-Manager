/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define, document, tau*/

/**
 * Main page module.
 *
 * @module views/main
 * @requires {@link core/template}
 * @requires {@link core/event}
 * @requires {@link helpers/popup}
 * @requires {@link helpers/dom}
 * @requires {@link models/filesystem}
 * @requires {@link models/state}
 * @namespace views/main
 */
define('views/main', [
        'core/template',
        'core/event',
        'helpers/popup',
        'helpers/dom',
        'models/filesystem',
        'models/state'
    ], function viewsMain(require) {
        'use strict';

        /**
         * Template core module.
         *
         * @memberof views/main
         * @private
         * @type {Module}
         */
        var template = require('core/template'),

            /**
             * Event core module.
             *
             * @memberof views/main
             * @private
             * @type {Module}
             */
            events = require('core/event'),

            /**
             * Toast helper module.
             *
             * @memberof views/main
             * @private
             * @type {Module}
             */
            popups = require('helpers/popup'),

            /**
             * DOM helper module.
             *
             * @memberof views/main
             * @private
             * @type {Module}
             */
            dom = require('helpers/dom'),

            /**
             * Filesystem module.
             *
             * @memberof views/main
             * @private
             * @type {Module}
             */
            filesystem = require('models/filesystem'),

            /**
             * Application state module.
             *
             * @memberof views/main
             * @private
             * @type {Module}
             */
            state = require('models/state'),

            /**
             * Reference to the page element.
             *
             * @memberof views/main
             * @private
             * @type {HTMLElement}
             */
            page = null,

            /**
             * Reference to the storages list.
             *
             * @memberof views/main
             * @private
             * @type {HTMLElement}
             */
            storagesList = null;

        /**
         * Handles click event on the storage list.
         * If clicked on the storage item, the storage will be opened.
         *
         * @memberof views/main
         * @private
         * @param {Event} event
         */
        function onStorageListClick(event) {
            var anchorEl = dom.findClosestElementByTag(event.target, 'A');

            if (anchorEl) {
                state.setCurrentPath(anchorEl.dataset.name);
                tau.changePage('#files');
            }
        }

        /**
         * Replaces the elements of the specified type to the given elements
         * in the storages list.
         * Clears the storages list and fills it with the given storages.
         *
         * @memberof views/main
         * @private
         * @param {object} storages
         * @param {FileSystemStorage[]} storages.internal
         * @param {FileSystemStorage[]} storages.external
         */
        function reloadStoragesList(storages) {
            storages.foundInternals = storages.internal.length > 0;
            storages.foundExternals = storages.external.length > 0;

            storagesList.innerHTML = template.get('storages-list-content',
                storages);
        }

        /**
         * Handles an array of storages released from the filesystem module.
         *
         * @memberof views/main
         * @private
         * @param {FileSystemStorage[]} storages
         */
        function onGetStorages(storages) {
            reloadStoragesList(storages);
        }

        /**
         * Opens a popup displaying the specified error.
         *
         * @memberof views/main
         * @private
         * @param {Error} error
         */
        function showErrorPopup(error) {
            popups.showToast(error.message);
        }

        /**
         * Handles the models.filesystem.storages.changed event.
         * Reloads the list of storages.
         *
         * @memberof views/main
         * @private
         * @param {Event} ev
         */
        function onStoragesChanged(ev) {
            reloadStoragesList(ev.detail);
        }

        /**
         * Initializes module.
         *
         * @memberof views/main
         * @public
         */
        function init() {
            page = document.getElementById('main');
            storagesList = document.getElementById('storages-list');
            storagesList.addEventListener('click', onStorageListClick);
            filesystem.getStorages().then(onGetStorages, showErrorPopup);
        }

        events.listeners({
            'models.filesystem.storages.changed': onStoragesChanged
        });

        return {
            init: init
        };
    }
);
