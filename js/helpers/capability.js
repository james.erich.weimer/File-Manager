/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define, tizen, console*/

/**
 * Capability helper module.
 *
 * @module helpers/capability
 * @namespace helpers/capability
 */
define('helpers/capability', function capability() {
    'use strict';

    /**
     * Keyboard capability key.
     * It allows the application to obtain the device capability information
     * using SystemInfo API.
     *
     * @memberof helpers/capability
     * @private
     * @const {string}
     */
    var KEYBOARD_CAPABILITY_KEY = 'http://tizen.org/feature/input.keyboard';

    /**
     * Checks if the keyboard is available.
     * Returns true it the keyboard is available, false otherwise.
     *
     * @memberof helpers/capability
     * @public
     * @returns {boolean}
     */
    function isKeyboardAvailable() {
        var result = false;

        try {
            result = tizen.systeminfo.getCapability(KEYBOARD_CAPABILITY_KEY);
        } catch (error) {
            console.error('Cannot check if the keyboard is available', error);
        }

        return result;
    }

    return {
        isKeyboardAvailable: isKeyboardAvailable
    };
});
