/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*global define, tizen, console*/

/**
 * Filesystem module.
 *
 * @module helpers/filesystem
 * @requires {@link core/event}
 * @requires {@link core/promise}
 * @requires {@link core/fs}
 * @namespace models/filesystem
 */
define('models/filesystem',
    ['core/event', 'core/promise', 'core/fs'],
    function filesystem(events, Promise, fs) {
        'use strict';

        /**
         * Internal storage type.
         *
         * @memberof models/filesystem
         * @private
         * @const {string}
         */
        var STORAGE_TYPE_INTERNAL = 'INTERNAL',

            /**
             * External storage type.
             *
             * @memberof models/filesystem
             * @private
             * @const {string}
             */
            STORAGE_TYPE_EXTERNAL = 'EXTERNAL',

            /**
             * Mounted storage state.
             *
             * @memberof models/filesystem
             * @private
             * @const {string}
             */
            STORAGE_STATE_MOUNTED = 'MOUNTED',

            /**
             * Private storages that should be filtered out from the list.
             *
             * @memberof models/filesystem
             * @private
             * @const {string[]}
             */
            PRIVATE_STORAGES = [
                'wgt-package',
                'wgt-private',
                'wgt-private-tmp'
            ],

            /**
             * Empty promise.
             *
             * @memberof models/filesystem
             * @private
             * @const {Promise}
             */
            EMPTY_PROMISE = new Promise(function emptyPromise(resolve) {
                resolve();
            }),

            /**
             * Sign that separates nodes in file path.
             *
             * @memberof models/filesystem
             * @public
             * @const {string}
             */
            SEPARATOR = '/';

        /**
         * Opens specified directory of file.
         * Returns promise resolving File object.
         *
         * @memberof models/filesystem
         * @private
         * @param {string} path
         * @returns {Promise<File>}
         */
        function getDirectory(path) {
            return fs.resolveFile(path);
        }

        /**
         * Filters and returns specified list of storages.
         * Excludes storages that:
         * 1. are unmounted
         * 2. have empty label
         * 3. are private
         * Labels of private storages are located in the PRIVATE_STORAGES array.
         *
         * @memberof models/filesystem
         * @private
         * @param {FileSystemStorage[]} storages
         * @returns {FileSystemStorage[]}
         */
        function filterStorages(storages) {
            var result = [],
                i = 0,
                length = storages.length,
                storage = null;

            for (; i < length; i += 1) {
                storage = storages[i];
                if (storage.label.length > 0 &&
                    storage.state === STORAGE_STATE_MOUNTED &&
                    PRIVATE_STORAGES.indexOf(storage.label) === -1) {
                    result.push(storage);
                }
            }

            return result;
        }

        /**
         * Splits storages into two categories: internal and external.
         * Returns object with these two categories.
         *
         * @memberof models/filesystem
         * @private
         * @param {FileSystemStorage[]} storages
         * @returns {
         *     {internal: FileSystemStorage[], external: FileSystemStorage[]}
         * }
         */
        function categorizeStorages(storages) {
            var external = [],
                internal = [],
                len = storages.length,
                i = 0,
                storage = null;

            for (i = 0; i < len; i += 1) {
                storage = storages[i];
                if (storage.type === STORAGE_TYPE_INTERNAL) {
                    internal.push(storage);
                } else if (storage.type === STORAGE_TYPE_EXTERNAL) {
                    external.push(storage);
                }
            }

            return {
                internal: internal,
                external: external
            };
        }

        /**
         * Obtains a list of storages and passes it through Promise.
         *
         * Filters obtained list by using the 'filterStorages' method
         * and then categorizes it by using the 'categorizeStorages' method.
         *
         * Returns Promise resolving object containing two lists
         * (internal and external storages).
         *
         * @memberof models/filesystem
         * @public
         * @returns {Promise<object>}
         */
        function getStorages() {
            return fs.getStorages().then(filterStorages)
                .then(categorizeStorages);
        }

        /**
         * Filters and returns specified file array.
         * Files that have empty name or are hidden are excluded from the list.
         *
         * @memberof models/filesystem
         * @private
         * @param {File[]} files
         * @param {boolean} includeHiddenFiles
         * @returns {File[]} Filtered array
         */
        function filterFiles(files, includeHiddenFiles) {
            var result = [],
                i = 0,
                length = files.length,
                file = null;

            for (; i < length; i += 1) {
                file = files[i];
                if (file.name !== '' &&
                    (includeHiddenFiles || file.name.charAt(0) !== '.')) {
                    result.push(file);
                }
            }

            return result;
        }

        /**
         * Obtains a list of files from directory handler
         * and passes it through Promise.
         *
         * Filters obtained list by using the 'filterFiles' method.
         *
         * Returns Promise instance resolving array of files.
         *
         * @memberof models/filesystem
         * @private
         * @param {File} directory
         * @param {boolean} includeHiddenFiles
         * @returns {Promise<Array>}
         */
        function listFiles(directory, includeHiddenFiles) {
            return new Promise(function promise(resolve, reject) {
                directory.listFiles(resolve, reject);
            }).then(function onGetFiles(files) {
                    return filterFiles(files, includeHiddenFiles);
                });
        }

        /**
         * Obtains a list of files from the specified path and passes it
         * through Promise.
         *
         * Returns Promise instance resolving array of files.
         *
         * @memberof models/filesystem
         * @public
         * @param {string} path
         * @param {boolean} [includeHiddenFiles]
         * @returns {Promise<Array>}
         */
        function getFiles(path, includeHiddenFiles) {
            return getDirectory(path)
                .then(function onGetFileListHandler(dir) {
                    return listFiles(dir, !!includeHiddenFiles);
                });
        }

        /**
         * Supports generateName function in creating unique name string.
         *
         * Searches a name of file/directory that does not exist in the
         * specified path and starts with the specified prefix.
         *
         * If the element with the name equal to prefix does not exists, the
         * prefix is returned. Otherwise the prefix with the " (nr)" suffix
         * is returned where nr is the number given as an input parameter.
         * If such file also exists the nr value is incremented and operation
         * is repeated as long as the unique name is found.
         *
         * Returns Promise instance resolving unique name string.
         *
         * @memberof models/filesystem
         * @private
         * @param {string} path
         * @param {string} prefix
         * @param {number} nr
         * @returns {Promise<string>}
         */
        function tryGenerateName(path, prefix, nr) {
            var name = prefix + (nr === 0 ? '' : ' (' + nr + ')');

            return fs.resolveFile(path + SEPARATOR + name).then(
                function onResolved() {
                    return tryGenerateName(path, prefix, nr + 1);
                }).catch(function onError(e) {
                    if (e.type === 'NotFoundError') {
                        return name;
                    } else {
                        throw e;
                    }
                });
        }

        /**
         * Checks if the file/folder located under the specified path exists.
         * It it does not exists the base name of this path is returned.
         * Otherwise the unique name is generated and returned. The generated
         * name is the base name of the path concatenated with the " (n)"
         * suffix, where "n" is the minimal integer that makes the name unique
         * in the specified path.
         * If the path ends with the " (n)" string, the new suffix is not
         * added but the number "n" of the existing one is incremented.
         *
         * Returns Promise instance resolving unique file name string.
         *
         * @example
         * If the path equals "documents/file.txt" and such file does not exists
         * in the filesystem, the returned value is "file.txt"
         *
         * @example
         * If the path equals "documents/file.txt" and such file exists in the
         * filesystem, the returned value is "file.txt (0)"
         *
         * @example
         * If the path equals "documents/file.txt" and in the filesystem exists
         * this file and the file "file.txt (0)", the returned value is
         * "file.txt (1)"
         *
         * @example
         * If the path equals "documents/file.txt (5)" and such file exists
         * in the filesystem, the returned value is "file.txt (6)"
         *
         * @memberof models/filesystem
         * @public
         * @param {string} path
         * @returns {Promise<string>}
         */
        function generateName(path) {
            var pathElements = path.split(SEPARATOR),
                fileName = pathElements.pop(),
                directoryPath = pathElements.join(SEPARATOR),
                regexResult = / \((\d+)\)$/.exec(fileName),
                nr = 0;

            if (regexResult) {
                fileName = fileName.substring(0,
                    fileName.length - regexResult[0].length);
                nr = parseInt(regexResult[1], 10);
            }
            return tryGenerateName(directoryPath, fileName, nr);
        }

        /**
         * Creates new empty file in a specified location.
         *
         * Obtains directory specified by given path
         * and passes it through Promise.
         *
         * Creates new file in the obtained directory
         * by using createFile method from Tizen Filesystem API.
         *
         * Returns Promise resolving object of the created file.
         *
         * @memberof models/filesystem
         * @public
         * @param {string} path
         * @returns {Promise<File>}
         */
        function createFile(path) {
            var directory = fs.getDirectoryPath(path),
                fileName = fs.basename(path);

            return getDirectory(directory)
                .then(function onGetDirectoryHandler(directory) {
                    return directory.createFile(fileName);
                });
        }

        /**
         * Creates new directory in a specified location.
         *
         * Obtains directory specified by path parameter
         * and passes it through Promise.
         *
         * Creates new directory in the obtained directory
         * by using createDirectory method from Tizen Filesystem API.
         *
         * Returns Promise resolving object of the created directory.
         *
         * @memberof models/filesystem
         * @public
         * @param {string} path
         * @returns {Promise<File>}
         */
        function createDirectory(path) {
            var directory = fs.getDirectoryPath(path),
                name = fs.basename(path);

            return getDirectory(directory)
                .then(function onGetDirectoryHandler(directory) {
                    return directory.createDirectory(name);
                });
        }

        /**
         * Moves file or directory from specified location to another.
         *
         * Based on given destinationPath, generates unique file name string
         * for moved file or directory by using the generateName method
         * and passes it through Promise.
         *
         * Uses generated name and moveTo method form Tizen Filesystem API
         * to move file or directory specified by the originPath parameter
         * to the destination specified by the destinationPath parameter.
         *
         * Returns Promise which lets the application to know
         * when the move operation is completed.
         *
         * @memberof models/filesystem
         * @private
         * @param {File} directory
         * @param {string} originPath Source path.
         * @param {string} destinationPath Path to which file will be moved.
         * @returns {Promise}
         */
        function moveTo(directory, originPath, destinationPath) {
            if (originPath === destinationPath) {
                return EMPTY_PROMISE;
            }

            var destinationDirectoryPath =
                destinationPath.split(SEPARATOR).slice(0, -1).join(SEPARATOR);

            return generateName(destinationPath)
                .then(function onGetName(fileName) {
                    return new Promise(function promise(resolve, reject) {
                        directory.moveTo(originPath,
                            destinationDirectoryPath + SEPARATOR + fileName,
                            false, resolve, reject);
                    });
                });
        }

        /**
         * Moves files or directories from specified location to another.
         *
         * Calls itself recursively, until originPaths array given as parameter
         * is empty.
         *
         * Each time it is called, it uses the moveTo method
         * to move the file or directory, specified by origin path
         * shifted form originPaths array parameter, to the destination
         * specified by destinationPath parameter.
         *
         * Returns Promise which lets the application know
         * when the move operation is completed.
         *
         * @memberof models/filesystem
         * @private
         * @param {File} directory
         * @param {string[]} originPaths Paths of the moved files/directories.
         * @param {string} destinationPath Destination directory path of the
         * moved files/directories.
         * @returns {Promise}
         */
        function moveFileListFromDirectory(directory, originPaths,
                destinationPath) {
            if (originPaths.length === 0) {
                return EMPTY_PROMISE;
            }

            var originPath = originPaths.shift();

            return moveTo(directory, originPath,
                destinationPath + SEPARATOR + fs.basename(originPath))
                .then(function onFileMoved() {
                    return moveFileListFromDirectory(directory,
                        originPaths, destinationPath);
                });
        }

        /**
         * Moves files or directories from specified location to another.
         *
         * Obtains directory using sample path from originPaths array
         * and passes it through Promise.
         *
         * Uses obtained directory to move files or directories
         * from location specified by originPaths array parameter
         * to destination specified by destinationPath parameter
         * by using moveFileListFromDirectory method.
         *
         * Returns Promise which lets the application know
         * when the move operation is completed.
         *
         * @memberof models/filesystem
         * @public
         * @param {string[]} originPaths Paths of the moved files/directories.
         * @param {string} destinationPath Destination directory path of the
         * moved files/directories.
         * @returns {Promise}
         */
        function moveFiles(originPaths, destinationPath) {
            if (originPaths.length === 0) {
                return EMPTY_PROMISE;
            }

            var directoryPath = fs.getDirectoryPath(originPaths[0]);

            return getDirectory(directoryPath)
                .then(function onGetDirectoryHandler(directory) {
                    return moveFileListFromDirectory(directory, originPaths,
                        destinationPath);
                });
        }

        /**
         * Renames the specified file/directory.
         *
         * Obtains directory specified by originPath parameter
         * and passes it through Promise.
         *
         * Uses obtained directory to rename file or directory
         * from location specified by originPath parameter
         * to the one specified by newName parameter
         * by using moveTo method from Tizen Filesystem API.
         *
         * Returns Promise which lets the application know
         * when the rename operation is completed.
         *
         * @memberof models/filesystem
         * @public
         * @param {string} originPath Full path to file.
         * @param {string} newName
         * @returns {Promise}
         */
        function rename(originPath, newName) {
            var currentDirectory = fs.getDirectoryPath(originPath),
                newDirectory = currentDirectory + SEPARATOR + newName;

            return getDirectory(currentDirectory)
                .then(function onGetDirectoryHandler(dir) {
                    return new Promise(function promise(resolve, reject) {
                        dir.moveTo(originPath, newDirectory, false,
                            resolve, reject);
                    });
                });
        }

        /**
         * Copies file or directory from specified location to another.
         *
         * Based on given destinationPath, generates unique file name string
         * for copied file or directory by using the generateName method
         * and passes it through Promise.
         *
         * Uses generated name and copyTo method form Tizen Filesystem API
         * to copy file or directory specified by the originPath parameter
         * to the destination specified by the destinationPath parameter.
         *
         * Returns Promise which lets the application know
         * when the copy operation is completed.
         *
         * @memberof models/filesystem
         * @private
         * @param {File} directory
         * @param {string} originPath Source path.
         * @param {string} destinationPath Path to which file will be copied.
         * @returns {Promise}
         */
        function copyTo(directory, originPath, destinationPath) {
            var destinationDirectoryPath =
                destinationPath.split(SEPARATOR).slice(0, -1).join(SEPARATOR);

            return generateName(destinationPath)
                .then(function onGetName(fileName) {
                    return new Promise(function promise(resolve, reject) {
                        directory.copyTo(originPath,
                            destinationDirectoryPath + SEPARATOR + fileName,
                            false, resolve, reject);
                    });
                });
        }

        /**
         * Copies files or directories from specified location to another.
         *
         * Calls itself recursively, until originPaths array given as parameter
         * is empty.
         *
         * Each time it is called, it uses the copyTo method
         * to copy the file or directory, specified by origin path
         * shifted form originPaths array parameter, to the destination
         * specified by destinationPath parameter.
         *
         * Returns Promise which lets the application know
         * when the copy operation is completed.
         *
         * @memberof models/filesystem
         * @private
         * @param {File} directory
         * @param {string[]} originPaths Array of paths from which file
         * will be copied.
         * @param {string} destinationPath Destination directory path of the
         * moved files/directories.
         * @returns {Promise}
         */
        function copyFileListFromDirectory(directory, originPaths,
                destinationPath) {
            if (originPaths.length === 0) {
                return EMPTY_PROMISE;
            }
            var originPath = originPaths.shift();

            return copyTo(directory, originPath,
                destinationPath + SEPARATOR + fs.basename(originPath))
                .then(function onFileCopied() {
                    return copyFileListFromDirectory(directory,
                        originPaths, destinationPath);
                });
        }

        /**
         * Copies files or directories from specified location to another.
         *
         * Obtains directory using sample path from originPaths array
         * and passes it through Promise.
         *
         * Uses obtained directory to copy files or directories
         * from location specified by originPaths array parameter
         * to destination specified by destinationPath parameter
         * by using copyFileListFromDirectory method.
         *
         * Returns Promise which lets the application know
         * when the copy operation is completed.
         *
         * @memberof models/filesystem
         * @public
         * @param {string[]} originPaths Array of paths from which file
         * will be copied.
         * @param {string} destinationPath Destination directory path of the
         * copied files/directories.
         * @returns {Promise}
         */
        function copyFiles(originPaths, destinationPath) {
            if (originPaths.length === 0) {
                return EMPTY_PROMISE;
            }

            var directoryPath = fs.getDirectoryPath(originPaths[0]);

            return getDirectory(directoryPath)
                .then(function onGetDirectoryHandler(directory) {
                    return copyFileListFromDirectory(directory, originPaths,
                        destinationPath);
                });
        }

        /**
         * Deletes the specified files.
         *
         * Calls itself recursively, until files array given as parameter
         * is empty.
         *
         * Each time it is called, it obtains directory
         * specified by path shifted from files array parameter
         * and passes it through Promise.
         * It also checks the type of the obtained file object
         * and depending on its type it uses
         * deleteDirectory or deleteFile methods from core/fs module
         * to remove it from filesystem.
         *
         * Returns Promise which lets the application know
         * when the delete operation is completed.
         *
         * @memberof models/filesystem
         * @private
         * @param {string[]} files
         * @param {File} directory
         * @returns {Promise}
         */
        function deleteFileListInDirectory(files, directory) {
            if (files.length === 0) {
                return EMPTY_PROMISE;
            }

            var filePath = files.shift();

            return getDirectory(filePath)
                .then(function onGetDirectoryHandler(file) {
                    if (file.isDirectory) {
                        return fs.deleteDirectory(directory, filePath, true);
                    } else {
                        return fs.deleteFile(directory, filePath);
                    }
                })
                .then(function onFileDeleted() {
                    return deleteFileListInDirectory(files, directory);
                });
        }

        /**
         * Deletes the specified files.
         *
         * Obtains directory using sample path from files array
         * and passes it through Promise.
         *
         * Uses obtained directory to remove files or directories
         * from location specified by files array parameter
         * by using deleteFileListInDirectory method.
         *
         * Returns Promise which lets the application know
         * when the delete operation is completed.
         *
         * @memberof models/filesystem
         * @public
         * @param {string[]} files
         * @returns {Promise}
         */
        function deleteFiles(files) {
            if (files.length === 0) {
                return EMPTY_PROMISE;
            }

            var directoryPath = fs.getDirectoryPath(files[0]);

            return getDirectory(directoryPath)
                .then(function onGetDirectoryHandler(directory) {
                    return deleteFileListInDirectory(files, directory);
                });
        }

        /**
         * Performs action if any changes in the device storages occurs.
         *
         * @memberof models/filesystem
         * @private
         * @param {FileSystemStorage[]} storages
         * @fires "models.filesystem.storages.changed"
         */
        function onStorageStateChanged(storages) {
            events.fire('storages.changed', {storages: storages});
        }

        /**
         * Returns parent directory path for given path.
         *
         * @memberof models/filesystem
         * @public
         * @param {string} path
         * @returns {string}
         */
        function getParentPath(path) {
            return fs.getDirectoryPath(path);
        }

        /**
         * Initializes module.
         *
         * @memberof models/filesystem
         * @public
         */
        function init() {
            try {
                tizen.filesystem.addStorageStateChangeListener(
                    onStorageStateChanged
                );
            } catch (error) {
                console.error('add storage state change listener error', error);
            }
        }

        /**
         * Opens a file located under the specified path
         * using a native application, which is suitable to open this file.
         *
         * Returns Promise which lets the application know
         * when the opening operation is completed.
         *
         * @memberof models/filesystem
         * @public
         * @param {string} path
         * @returns {Promise}
         */
        function openFile(path) {
            var appControl = new tizen.ApplicationControl(
                'http://tizen.org/appcontrol/operation/view',
                path
            );

            return new Promise(function promise(resolve, reject) {
                try {
                    tizen.application.launchAppControl(appControl, null,
                        resolve, reject);
                } catch (error) {
                    console.error('launch app control error', error);
                }
            });
        }

        return {
            init: init,
            getStorages: getStorages,
            getFiles: getFiles,
            createFile: createFile,
            createDirectory: createDirectory,
            rename: rename,
            moveFiles: moveFiles,
            copyFiles: copyFiles,
            deleteFiles: deleteFiles,
            getParentPath: getParentPath,
            generateName: generateName,
            openFile: openFile,

            SEPARATOR: SEPARATOR
        };
    }
);
